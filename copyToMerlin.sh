#!/bin/sh

if [ -z $1 ]
then
    echo ERROR no path;
    exit;
fi

#ssh vla0054@merlin1.cs.vsb.cz rm -rf /hom/fei/vla0054/$1
scp -r $1 vla0054@merlin2.cs.vsb.cz:/home/fei/vla0054/$1

