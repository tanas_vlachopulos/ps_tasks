#include <iostream>
#include <string>
#include <chrono>
#include "md5.h"
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <cmath>
using namespace std;

#define rangeSize ('z' - 'A' + 1)

string i2word(int i) {
	
		string word;
		int pi = i;
		for (;;) {
			word.insert(0, 1, (char) ('A' + (pi % rangeSize) ));
			pi /= rangeSize;
			if (pi == 0) {
				break;
			}
		}

		return word;
}

void crack(string hash, int start, int count, string *&result)
{
	cout << i2word(start) << endl;
	
    for (int i = start; i < start+count; i++) {
	
		if (md5(i2word(i)).compare(hash) == 0) {
			result = new string (i2word(i));
			return;
        }
    }
}

void work(string hash, int maxSize, int forkCount, string *&result)
{
	int pidCount = 0;
	int pids[100000];

	long long int cyc = rangeSize;
	for (int ix = 2; ix <= maxSize; ix++) {
		cyc *= rangeSize;
	}

	int perCycle = 10000000;
	
	for (int i = 0; i < (cyc / perCycle)+1; i++) {
		int pid;
		pid = fork();
		if (pid == 0) {

			crack(hash, perCycle * i, perCycle, result);
			
			if (result != NULL) {
				cout << "FOUND:" << *result << endl;
				exit(1);
			}
			
			return;
		} else {
			pids[pidCount] = pid;
			pidCount += 1;
			
			cout << pidCount << endl;
			
			if (pidCount % forkCount == 0) {
				int status;
				for (int y = pidCount - forkCount; y < pidCount; y++) {
					cout << "waits...";
					waitpid(pids[y],&status,0);
					cout << " status: " << WEXITSTATUS(status);
					if (WEXITSTATUS(status) == 1) {
						cout << "!!" << endl;
						return;
					}
					
					cout << endl;
				}
				cout << endl;
			}
		}
	}
}

void test(string hash, int max, int forkCount) {

    cout << "Start size=" << max << "; forkCount=" << forkCount << ":" << endl;

    auto start = std::chrono::high_resolution_clock::now();

    string* result = NULL;
    work(hash, max, forkCount, result);

    if (result != NULL) {
        cout << "FOUND:" << *result << endl;
    }

    auto finish = std::chrono::high_resolution_clock::now();
    auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish-start);
    std::cout << microseconds.count() << " nano s\n";

    delete result;
}

int main(int argc, char* argv[]) {
	string row = md5(argv[1]);
    test(row, atoi(argv[2]), atoi(argv[3]));
	
	return 0;
}

