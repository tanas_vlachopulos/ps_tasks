#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/prctl.h>
#include <signal.h>
#include "md5.h"

// generate ASCII alfabet
char *generate_alfabet()
{
    // const unsigned int alfabetLen = 94;
    // const unsigned int offset = 42; // first ascii character
    // const unsigned int end = 127;   // last ascii character
    // ASCII table: https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/ASCII-Table-wide.svg/2000px-ASCII-Table-wide.svg.png
    const unsigned int offset = (unsigned int)'*';  // first ascii character
    const unsigned int end = (unsigned int)'~' + 1; // last ascii character
    static char alfabet[end - offset + 1];

    for (int i = offset; i < end; i++)
    {
        alfabet[i - offset] = (char)i;
    }

    return alfabet;
}

// compare two hash
// return true if match
bool compare_hash(unsigned char *value1, unsigned char *value2)
{
    return (!strcmp((char *)value1, (char *)value2));
}

// callback function for kill_them_all signal
void die_child_die(int sig)
{
    if (sig == SIGUSR1)
    {
        exit(0);
    }
}

// send SIGUSR1 signal to all proces child
void kill_them_all(int child_count, pid_t *pids)
{
    for (int i=0; i < child_count; i++)
    {
        kill(pids[i], SIGUSR1);
    }
}

// print result of hashing function
void print_digest(unsigned char *word, unsigned char *digest)
{
    printf("Match: %s -> ", word);
    for (int ii = 0; ii < 16; ii++)
    {
        printf("%02x", digest[ii]);
    }
    printf("\n");
}

// search iteration for given word
// iteration is designed to be child process
void search_iteration(char word[], char *alfabet, unsigned char *hash, unsigned int position, unsigned int word_len)
{
    if (position < word_len)
    {
        for (int i = 0; i < strlen(alfabet); i++)
        {
            word[position] = alfabet[i];
            if (strlen(word) > position + 1)
            {
                word[position + 1] = 0;
            }
            // osetreni prvni iterace, kdy je znak fixni
            // word[position] = position == 0 ? word[0] : alfabet[i];

            unsigned char *digest = do_md5((unsigned char *)word, strlen(word));

            if (compare_hash(hash, digest))
            {
                print_digest((unsigned char *)word, digest);

                exit(2);
            }
            // printf("%s   position: %d\n", word, position); // debug

            free(digest);

            // recursion
            search_iteration(word, alfabet, hash, position + 1, word_len);
        }
    }
    else
    {
        // printf("end\n");
        return;
    }
}

void find_word_for_hash(unsigned char *hash, unsigned int word_len)
{
    char *alfabet;
    alfabet = generate_alfabet();

    char word[256] = {0};
    // printf("alfabet len %lu\n", strlen(alfabet));

    pid_t child_pid, wpid;
    pid_t child_pids[255];
    int retcode = 0;
    int child_count = 0;
    for (int i = 0; i < strlen(alfabet); i++)
    {
        if ((child_pid = fork()) == 0)
        {
            // printf("Fork for letter -> %c\n", alfabet[i]); // DEBUG
            signal(SIGUSR1, die_child_die);

            word[0] = alfabet[i];
            unsigned char *digest = do_md5((unsigned char *)word, strlen(word));
            if (compare_hash(hash, digest))
                print_digest((unsigned char *)word, digest);

            search_iteration(word, alfabet, hash, 1, word_len);

            exit(0);
        }
        else
        {
            child_pids[i] = child_pid;
            child_count++;
        }
    }

    while ((wpid = wait(&retcode)) > 0)
    {
        if (retcode != 0)
        {
            printf("Child found hash\n");
            kill_them_all(child_count, child_pids);
        }
    }
}

int main(int argc, char *argv[])
{
    // time measurement variables
    struct timeval diff, start, end;

    if (argc < 1)
    {
        printf("Message not found");
        return 1;
    }

    unsigned int message_len = strlen(argv[1]);

    printf("MD5 of %s\n", argv[1]);

    unsigned char *digest = do_md5((unsigned char *)argv[1], message_len);

    for (int i = 0; i < 16; i++)
    {
        printf("%02x", digest[i]);
    }
    printf("\n");

    gettimeofday(&start, NULL); // get start time

    find_word_for_hash(digest, message_len);

    gettimeofday(&end, NULL);      // get end time
    timersub(&end, &start, &diff); // calc time difference
    printf("Elapsed time = %ld.%ld sec\n", diff.tv_sec, diff.tv_usec);

    free(digest);
    return 0;
}