#include <iostream>
#include <string>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <cmath>
#include <ctime>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/prctl.h>
#include <stdio.h>
#include <signal.h>
#include "md5.h"

using namespace std;

#define rangeSize ('z' - 'A' + 1)

#define hashType unsigned char[16]

void run_mult(unsigned char *hash, int *result, int start, int Length);

string convert_int_to_word(int i) {
	
		string word;
		int pi = i;
		for (;;) {
			word.insert(0, 1, (char) ('A' + (pi % rangeSize) ));
			pi /= rangeSize;
			if (pi == 0) {
				return word;
			}
		}

		return word;
}

int convert_hex_to_string(unsigned char *data, const unsigned char *hexstring, unsigned int len)
{
    unsigned const char *pos = hexstring;
    char *endptr;
    size_t count = 0;

    if ((hexstring[0] == '\0')) {
        //hexstring contains no data
        //or hexstring has an odd length
        return -1;
    }

    for(count = 0; count < len; count++) {
        char buf[5] = {'0', 'x', pos[0], pos[1], 0};
        data[count] = strtol(buf, &endptr, 0);
        pos += 2 * sizeof(char);

        if (endptr[0] != '\0') {
            //non-hexadecimal character encountered
            return -1;
        }
    }

    return 0;
}

// Function prototype from .cu file
void run_mult( unsigned char* hash, int *result, int start, int Length );

#define N (1024 * 500)

int main(int argc, char *argv[])
{
	if (argc < 1)
    {
        printf("Message not found");
        return 1;
    }

	struct timeval diff, start, end;

	unsigned int messageLen = strlen(argv[1]);	

	unsigned char *digest = do_md5((unsigned char *)argv[1], messageLen);

	// print hash
	for (int i = 0; i < 16; i++)
    {
        printf("%02x", digest[i]);
    }
    printf("\n");

	// unsigned char row[16];	
	// convert_hex_to_string(row, digest, 16);
	

	// Array initialization 
	long long int cudaIteration = rangeSize;
	for (int ix = 2; ix <= messageLen; ix++) {
		cudaIteration *= rangeSize;
	}
	
	int found = 0;

    gettimeofday(&start, NULL); // get start time

	
	for (int i = 0; i < (cudaIteration / N)+1; i++) {

		int * words;
		words = new int[N];		
		run_mult(digest, words, N * i, N);

		for ( int y = 0; y < N; y++ ) {
			if (words[ y ] != 0) {
				found = N * i + y;
				break;
			}
		}
		if (found != 0) {
			break;
		}
	}
	cout << "Original word: " << convert_int_to_word(found) << endl;
	
	gettimeofday(&end, NULL);      // get end time
    timersub(&end, &start, &diff); // calc time difference
    printf("Elapsed time = %ld.%ld sec\n", diff.tv_sec, diff.tv_usec);
	
	return 0;
}

