// ***********************************************************************
//
// Demo program for education in subject
// Computer Architectures and Paralel Systems
// Petr Olivka, dep. of Computer Science, FEI, VSB-TU Ostrava
// email:petr.olivka@vsb.cz
//
// Example of CUDA Technology Usage
// Multiplication of elements in float array
//
// ***********************************************************************
#include <iostream>
#include <string>
//#include <chrono>
//#include "md5_c.h"
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <cmath>
#include <ctime>
using namespace std;

#define rangeSize ('z' - 'A' + 1)

#include <stdio.h>
#define hashType unsigned char[16]

string i2word(int i) {
	
		string word;
		int pi = i;
		for (;;) {
			word.insert(0, 1, (char) ('A' + (pi % rangeSize) ));
			pi /= rangeSize;
			if (pi == 0) {
				return word;
			}
		}

		return word;
}

int hex2data(unsigned char *data, const unsigned char *hexstring, unsigned int len)
{
    unsigned const char *pos = hexstring;
    char *endptr;
    size_t count = 0;

    if ((hexstring[0] == '\0')) {
        //hexstring contains no data
        //or hexstring has an odd length
        return -1;
    }

    for(count = 0; count < len; count++) {
        char buf[5] = {'0', 'x', pos[0], pos[1], 0};
        data[count] = strtol(buf, &endptr, 0);
        pos += 2 * sizeof(char);

        if (endptr[0] != '\0') {
            //non-hexadecimal character encountered
            return -1;
        }
    }

    return 0;
}

// Function prototype from .cu file
void run_mult( unsigned char* hash, int *result, int start, int Length );

#define N (1024 * 500)

int main()
{
	unsigned char* texts[7];
	texts[0] = (unsigned char*) "fbade9e36a3f36d3d676c1b808451dd7";
	texts[1] = (unsigned char*) "25ed1bcb423b0b7200f485fc5ff71c8e";
	texts[2] = (unsigned char*) "f3abb86bd34cf4d52698f14c0da1dc60";
	texts[3] = (unsigned char*) "02c425157ecd32f259548b33402ff6d3";
	texts[4] = (unsigned char*) "95ebc3c7b3b9f1d2c40fec14415d3cb8";
	texts[5] = (unsigned char*) "453e41d218e071ccfb2d1c99ce23906a";
	
	
    std::clock_t start;
    double duration;
	
	for (int ip = 0; ip < 7; ip++) {
		
		unsigned char row[16];
		//unsigned char data[] = "8c01360b2dba58b127cf2fb30ab8981d";
		
		hex2data(row, texts[ip], 16);
		
		unsigned int maxSize = 6;

		// Array initialization 
		long long int cyc = rangeSize;
		for (int ix = 2; ix <= maxSize; ix++) {
			cyc *= rangeSize;
		}
		
		int found = 0;

			
		start = std::clock();

		
		for (int i = 0; i < (cyc / N)+1; i++) {

			int * prvky;
			prvky = new int[N];		
			run_mult(row, prvky, N * i, N);

			for ( int y = 0; y < N; y++ ) {
				if (prvky[ y ] != 0) {
					found = N * i + y;
					break;
				}
			}
			if (found != 0) {
				break;
			}
		}
		cout << "FOUND: " << ip << ":" << i2word(found) << endl;
		
			

		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

		std::cout<<"printf: "<< duration <<'\n';

	}
	
	return 0;
}

