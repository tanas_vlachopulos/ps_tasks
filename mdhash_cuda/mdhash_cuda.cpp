#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "md5.h"


int main(int argc, char *argv[])
{
    if (argc <= 1)
    {
        printf("Missing argument");
        return 0;
    }

    uint p1, p2, p3, p4;

    unsigned char hash[32];
	memcpy(hash, "f0e8fb430bbdde6ae9c879a518fd895f", 32);

    md5_to_ints(hash,  &p1, &p2, &p3, &p4);
    printf("%s \n%d %d %d %d", hash, p1, p2, p3, p4);
    

    // md5_vfy((unsigned char *) argv[1], strlen(argv[1]))
}