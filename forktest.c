#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

void iteration(int size, int max_depth, int depth, int id)
{
    for (int i = 0; i < size; i++)
    {
        printf("Depth: %d, Iteration ID = %d\n", depth, id + i);

        if (depth < max_depth)
        {
            iteration(size, max_depth, depth += 1, id += i);
        }
    }
}

void forking()
{
    for (int i = 0; i < 10; i++)
    {
        if (fork() == 0)
        {
            printf("fork %d, pid: %d\n", i, getpid());

            exit(0);
        }
    }
}

int main(void)
{
    pid_t pid[5] = {0};
    int retcode;

    for (int i = 0; i < 5; i++)
    {
        pid[i] = fork();

        if (pid == 0)
        {
            printf("Potomek...\n");
            sleep(1);
            printf("Potomek skoncil\n");
            exit(99);
        }
        else if (pid < 0)
        {
            printf("Fork selhal\n");
            exit(2);
        }
        else
        {
            exit(0);
        }
    }

    for (int i = 0; i < 5; i++)
    {
        printf("Rodic zna PID potomka: %d...a ceka na jeho konec\n", pid[i]);
    }
    waitpid(-1, &retcode, 0);
    printf("Potomek skoncil s kodem: %d\n", WEXITSTATUS(retcode));
    printf("Rodic konci\n");
    exit(3);

    exit(1);
}