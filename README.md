# Počítačové systémy
## CV01 & CV02 - výpočet MD5 hashe a zpětné nalezení řetězce
**mdhash3.c** -
Vypočte z řetězce znaků MD5 hash a následně nalezne pro vypočtený hash původní řetězec znaků.
Jako argument přijimá řetězec k převedení.
Maximální délka řetězce je 255, ale při více než 5 znacích trvá převod velmi dlouho.

Kompilace programu pomocí řádkového překladače:
`g++ -o mdhash3 mdhash3.c`

Spouštění:
`./mdhash3 retezec_k_prevedeni`

## CV03 - multiprocesov7 výpočet MD5 hashe
**mdhash_multithread.c** -
Stejný jako mdhash3.c, ale počítá hashe ve více vláknech pomocí pthread. 

Kompilace programu pomocí řádkového překladače:
`g++ -o mdhash3 -pthread mdhash3.c`

Spouštění:
`./mdhash3 retezec_k_prevedeni pocet_vlaken` (výchozí počet vláken jsou 4)

**mdhash_fork** - počítá hashe ve více procesech pomocí funkce fork. Složka obsahuje následující soubory:

*md5.h* - obsahuje funkce pro výpočet MD5 hashe

*mdhash_fork* - implementace reverzního zjišťování ve více procesech

*makefile* - předpis pro kompilaci funkce pomocí makefile

Kompilace programu: ve složce mdhash_fork zavolat `make`

Spuštění: `./mdhash_fork retezec_k_prevedeni`

Aplikace je omezena na 255 znaků, ale na běžném počítači lze počítat v rozumném čase pouze 4 znakové slova, v clusteru až 6 (2167 s).

## CV04 - Clustering
Spuštění multiprocesorové aplikace v clusteru: `mosrun ./mdhash_fork params`

Přehled vytíženosti clusteru: `mosmon`
