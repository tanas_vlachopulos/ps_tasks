#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "md5.h"

void run_mult(char *words, int length, unsigned char *hash);
void copy(unsigned char *allWords, char *word, int wordLen);
char *generate_alfabet();
unsigned char *generate_words(char *alfabet, int len);
void generate_iteration(char *alfabet, unsigned char *words, unsigned int position, unsigned int word_len);


int iteration = 0;
int wordsLen = 0;

char *generate_alfabet()
{
    // const unsigned int alfabetLen = 94;
    // const unsigned int offset = 42; // first ascii character
    // const unsigned int end = 127;   // last ascii character
    // ASCII table: https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/ASCII-Table-wide.svg/2000px-ASCII-Table-wide.svg.png
    const unsigned int offset = (unsigned int)'a';  // first ascii character
    const unsigned int end = (unsigned int)'z' + 1; // last ascii character
    // static char alfabet[end - offset + 1];
    char *alfabet = new char[end - offset + 1];

    for (int i = offset; i < end; i++)
    {
        alfabet[i - offset] = (char)i;
    }

    return alfabet;
}

void copy(unsigned char *allWords, char *word, int wordLen)
{
    for (int i = 0; i < wordLen; i++)
    {
        allWords[(iteration * wordsLen) + i] = (unsigned char)word[i];
    }
}

void generate_iteration(char *alfabet, unsigned char *words, char *word, unsigned int position, unsigned int word_len)
{
    // char word[256] = {0};
    if (position < word_len)
    {
        for (int i = 0; i < strlen(alfabet); i++)
        {
            word[position] = alfabet[i];

            // printf("%s\n", word);
            
            copy(words, word, word_len);
            iteration++;

            // recursion
            generate_iteration(alfabet, words, word, position + 1, word_len);
        }
    }
    else
    {
        // printf("end\n");
        return;
    }
}

// alfabet - string contain alfabet
// len - max length of words
unsigned char *generate_words(char *alfabet, int len)
{
    int count = pow(strlen(alfabet), len) * len;
    
    unsigned char *words = new unsigned char[count];

    char word[256] = {0};
    generate_iteration(alfabet, words, word, 0, len);

    return words;
}

int main(int argc, char *argv[])
{
    if (argc <= 1)
    {
        printf("Missing argument\n");
        return 0;
    }
    wordsLen = strlen(argv[1]);

    char *alfabet = generate_alfabet();

    // for (int i = 0; i < strlen(alfabet); i++)
    // {
    //     printf("%c\n", alfabet[i]);
    // }

    unsigned char *hash = do_md5((unsigned char *)argv[1], wordsLen);
    print_md5(hash);

    unsigned char *words = generate_words(alfabet, wordsLen);

    // printf("Size: %d\n", strlen((char*)words));
    // for (int i = 0; i < (pow(strlen(alfabet), wordsLen) * wordsLen); i++)
    // {
    //     if ((i % wordsLen) == 0)
    //         printf("\n");
    //     printf("%c", words[i]);
    // }
    // printf("\n");

    run_mult(words, (pow(strlen(alfabet), wordsLen) * wordsLen), wordsLen, hash);

    free(alfabet);
    free(hash);
}